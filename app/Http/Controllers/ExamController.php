<?php

namespace App\Http\Controllers;

use App\Models\Batch;
use App\Models\Exam;
use App\Models\Answer;
use App\Models\SectionCategory;
use App\Models\Subject;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $allExams = !Auth::user()->batch? null : Auth::user()->batch->exams()->get()->sortByDesc('created_at');
        $notifications = !Auth::user()->batch? null : Auth::user()->batch->notifications()->orderBy('id','desc')->get();
        $downloads = !Auth::user()->batch? null : Auth::user()->batch->downloads()->orderBy('id','desc')->get();
        $answers = Auth::user()->answers()->with('exam')->get()->sortByDesc('created_at');
//      $subjects = Subject::all();

//      dd(Auth::user()->batch->notifications()->orderBy('id','desc')->get()->toArray());
//        dd(Batch::find(4)->notifications()->get());
//      $upcomingExams = !Auth::user()->batch? null : Auth::user()->batch->exams()->where( 'start_date_time', '>', Carbon::now())->where('start_date_time', '<', Carbon::today()->addDays(30))->get();
//
//      dd(Auth::user()->batch->exams()->where('end_date_time', '<', Carbon::today())->get());
//
//      $expiredExams = !Auth::user()->batch? null : Auth::user()->batch->exams()->where('end_date_time', '<', Carbon::today())->get();
//
//      $liveExams = !Auth::user()->batch? null : Auth::user()->batch->exams()->where('start_date_time', '<', Carbon::now())->where('end_date_time', '>', Carbon::now() )->get();

        return view('exams.index', compact('allExams','notifications', 'downloads', 'answers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Exam $exam)
    {

        $now = date('Y-m-d G:II');
        $instructions = $exam->questionPaper->instructions? : $exam->questionPaper->instruction->contents;
        $sections = SectionCategory::all();

        $exam->load(['examCategory','questionPaper'=> function($query){
            return $query->select('id','name','questions', 'answer_key_url', 'created_at');
        }]);

        return view('exams.show', compact('exam','now','instructions','sections'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
