<?php

namespace App\Http\Controllers\Admin;

use App\Models\Exam;
use App\Models\Instruction;
use App\Models\QuestionPaper;
use App\Models\Tag;
use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\ExamRequest as StoreRequest;
use App\Http\Requests\ExamRequest as UpdateRequest;
use App\Models\ExamCategory;
use App\Models\ExamType;
use App\Models\Question;
use App\Models\SectionCategory;
use App\Models\Subject;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Imports\QuestionsImport;

class ExamCrudController extends CrudController
{
    public function setup()
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Exam');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/exam');
        $this->crud->setEntityNameStrings('Exam', 'Exams');
        $this->crud->orderBy('created_at', 'DESC');

        $this->crud->addButtonFromView('top', 'importExcel', 'importExcelButton', 'end');
        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

//        $this->crud->setFromDb();

        $this->crud->addFields([
            [
                'name'=>'name',
                'label'=>'Name',
            ],
            [
                'name'=> 'batches',
                'label'=> 'Batch',
                'hint'=> 'Only the batches selected above, can write this exam',
                'type'=> 'checklist',
                'entity'=> 'batches',
                'attribute'=> 'name',
                'model'=> 'App\Models\Batch',
                'pivot'=> true,
                'select_all' => true,
            ],
//            [
//                'name'=> 'examTypes',
//                'label'=> 'Exam Type',
//                'type'=> 'checklist',
//                'entity'=> 'examTypes',
//                'attribute'=> 'name',
//                'model'=> 'App\Models\ExamType',
//                'pivot'=> true,
//                'select_all' => true,
//            ],

            [
                'name'=>'start_date_time',
                'label'=>'Start Date/Time',
                'type' => 'datetime_picker'
            ],
            [
                'name'=>'end_date_time',
                'label'=>'End Date/Time',
                'type' => 'datetime_picker'
            ],
            [
                'name'=>'duration',
                'label'=>'Overall Exam Duration in minutes (If you want this exam to be an Open-Section Exam!)',
                'attributes' => ['placeholder'=> 'Leave this field blank, if you need section-based time for this exam (Closed-section exam)'],
                'type' => 'number',
                'hint'=> 'Note: Closed-section timings are set in the Question paper itself. Leave this field blank, if you have set time for each section of this question paper.'
            ],

            [
                'name'=> 'difficulty',
                'label'=> 'Difficulty',
                'type' => 'select_from_array',
                'options' => ['easy'=>'Easy', 'medium'=>'Medium', 'difficulty' => 'Difficult'],
                'attribute'=>'name',
                'allows_null' => false,
                'hint'=> ''
            ],
            [
                'name'=>'marks_per_question',
                'label'=>'Marks per Question (Optional)',
                'default' => '1',
                'type' => 'number',
                'hint'=> 'If you have NOT assigned marks to ALL questions in the selected Question paper, then this field value will be assigned as marks for those marks-unassigned questions'
            ],
            [
                'name'=>'negative_marks',
                'label'=>'Negative marks for each wrong answer',
                'default' => '1',
                'type' => 'number',
                'hint'=> 'Leave blank or enter 0, if no negative marking is required',
                'attributes'=>['step'=>'0.05'],
                'hint'=> 'For negative marks, this field value will be used for this exam. Commonly used values are 0.25, 0.33 or 1'

            ],
            [
                'name'=>'max_entries',
                'label'=>'Total No. of Students permitted to attend this exam',
                'type' => 'number',
                'hint'=> 'Leave blank for allowing unlimited students'
            ],
            [
                'name'=>'exam_category_id',
                'label'=>'Exam Category',
                'type' => 'select',
                'entity'=> 'examCategory',
                'model'=> 'App\Models\ExamCategory',
                'placeholder'=> 'Select One Category',
                'attribute'=> 'name'
            ],

        ], 'update/create/both');

        $this->crud->addFields([
            [
                'name'=>'sms_notifications',
                'label'=>'Send SMS Notifications to attendees after exam',
                'type' => 'checkbox',
                'default'=> true
            ],
            [
                'name'=>'display_answer_key',
                'label'=>'Display answer key to students after exam',
                'type' => 'checkbox',
                'default'=> true

            ],
            [
                'name'=>'accept_instructions',
                'label'=>'Make Students mandatorily accept the Instructions before proceeding',
                'type' => 'checkbox',
                'default'=> true
            ],
            [
                'name'=>'allow_submit',
                'label'=>'Allow students to submit the exam before the allocated time',
                'type' => 'checkbox',
                'default'=> false
            ],
            [
                'name'=>'shuffle',
                'label'=>'Shuffle questions',
                'type' => 'checkbox',
                'default'=> false,
                'hint'=> 'The order of the sections will remain same, shuffling will happen only within questions in the same section'
            ],
        ], 'update/create/both');

        $this->crud->addFields([
            [ // Table
                'name' => 'question_paper_id',
                'label' => 'Question Paper',
                'type' => 'select',
                'entity'=> 'questionPaper',
                'entity_singular' => 'Question Paper', // used on the "Add X" button
                'model' => 'App\Models\QuestionPaper',
                'attribute'=> 'name',
                'attributes'=> ['id'=>'questionPaper']
            ],
            [
                'name'=> 'cut_off_marks',
                'label'=> 'Overall Cut-off Marks (Optional)',
                'type'=>'number',
                'attributes'=> ['min'=>1],
                'hint'=> 'If you leave this field bank, then the section-wise cut-off(average marks of students in each section) will be applied automatically'
            ],
            [
                'name'=> 'passed_message',
                'label'=> 'Custom Message to display if exam is passed (Optional)',
                'type'=>'text',
                'hint'=> ' '
            ],
            [
                'name'=> 'failed_message',
                'label'=> 'Custom Message to display if exam is failed (Optional)',
                'type'=>'text',
                'hint'=> ' '
            ]
        ]);


        $this->crud->addColumns([

            [
                'name'=> 'name',
                'label'=> 'Name'
            ],
            [
                'name'=> 'duration',
                'label'=> 'Set Duration (min)'
            ],
//            [
//                'name'=> 'cut_off_marks',
//                'label'=> 'Cut-Off'
//            ],
            [
                'name'=> 'start_date_time',
                'label'=> 'Start date/time',
                'type'=> 'date'
            ],
            [
                'name'=> 'end_date_time',
                'label'=> 'End date/time',
                'type'=> 'date'
            ]

        ]);


//
//         $this->crud->addField([
//             'name' => 'custom-ajax-button',
//             'type' => 'view',
//             'view' => 'auth.login'
//         ], 'update/create/both');





        // ------ CRUD FIELDS
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
//        dd($request);
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function importExcel()
    {

        $this->crud->removeAllFields();
        $examTypes = ExamType::all()->toArray();
        $examTypeOptions= [];
        $examCategories = ExamCategory::all()->toArray();
        $examCategoriesOptions= [];

        foreach ($examTypes as $type){
            $examTypeOptions[$type['id']] = $type['name'];
        }

        foreach ($examCategories as $cat){
            $examCategoriesOptions[$cat['id']] = $cat['name'];
        }

//        dd('$examTypes', $examTypeOptions);
        $this->crud->addFields([
            [   // Upload
                'name' => 'questionsFile',
                'label' => 'Upload Excel Sheet',
                'type' => 'upload',
                'upload' => true,
                'disk' => 'uploads', // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;,
                'hint'=> 'A new Exam and Question paper will be created from this excel sheet. You can edit those after saving this form'
            ],
            [
                'name' => 'name',
                'label' => 'New Exam Name*',
                'hint'=> 'A new Exam will be created'
            ],
            [
                'name' => 'questionPaperName',
                'label' => 'New Question Paper Name*',
                'hint'=> 'A new Question paper will be created with the default instructions set from Instructions Master Settings.'

            ],
            [
                'name'=> 'difficulty',
                'label'=> 'Difficulty',
                'type' => 'select_from_array',
                'options' => ['easy'=>'Easy', 'medium'=>'Medium', 'difficulty' => 'Difficult'],
                'attribute'=>'name',
                'allows_null' => false,
                'hint'=> ''
            ],
            [
                'name' => 'examTypes',
                'label' => "Exam Type*",
                'type' => 'select2_from_array',
                'options' => $examTypeOptions,
                'allows_null' => false,
                'default' => $examTypes[0]['id'],
                'allows_multiple' => true, // OPTIONAL; needs you to cast this to array in your model;
            ],
            [
                'name'=> 'batches',
                'label'=> 'Batch',
                'hint'=> 'Only the batches selected above, can write this exam',
                'type'=> 'checklist',
                'entity'=> 'batches',
                'attribute'=> 'name',
                'model'=> 'App\Models\Batch',
                'pivot'=> true,
                'select_all' => true,
            ],
            [
                'name'=>'start_date_time',
                'label'=>'Start Date/Time*',
                'type' => 'datetime_picker'
            ],
            [
                'name'=>'end_date_time',
                'label'=>'End Date/Time*',
                'type' => 'datetime_picker'
            ],
            [
                'name'=>'duration',
                'label'=>'Duration (in minutes)*',
                'attributes' => ['placeholder'=> 'Leave this field blank, if you need session-based time for this exam'],
                'type' => 'number',
                'hint'=> 'If you enter the above field, then this value will be regarded as the overall duration of the exam, and this will be an open-session exam, which means no individual time-limits will be set for each section'
            ],
            [
                'name'=>'marks_per_question',
                'label'=>'Marks per Question',
                'default' => '1',
                'type' => 'number',
                'hint'=> 'If set, this value will override the right marks set for each question'

            ],
            [
                'name'=>'negative_marks',
                'label'=>'Negative marks for each wrong answer',
                'default' => '1',
                'type' => 'number',
                'hint'=> 'Leave blank or enter 0, if no negative marking is required',
                'attributes'=>['step'=>'0.05'],
                'hint'=> 'If set, this value will override the negative marks set for each question'

            ],
            [
                'name'=>'max_entries',
                'label'=>'Total No. of Students permitted to attend this exam',
                'type' => 'number',
                'hint'=> 'Leave blank for allowing unlimited students'
            ],
            [
                'name'=>'exam_category_id',
                'label'=>'Exam Category*',
                'type' => 'select_from_array',
                'options' => $examCategoriesOptions,
                'allows_null' => false,
                'default' => $examCategories[0]['id'],
                'allows_multiple' => false,
            ],
//            [   // Upload
//                'name' => 'questionsFile2',
//                'label' => 'Upload Excel Sheet',
//                'type' => 'upload',
//                'upload' => true,
//                'disk' => 'uploads' // if you store files in the /public folder, please ommit this; if you store them in /storage or S3, please specify it;
//            ],
        ]);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->getSaveAction();
        $this->data['fields'] = $this->crud->getCreateFields();
//        $this->data['title'] = $this->crud->getTitle() ?? trans('backpack::crud.add').' '.$this->crud->entity_name;
        $this->data['title'] = 'Exam - Bulk Import Questions from Excel';

        return view('vendor.backpack.crud.importExcel', $this->data);
    }

    public function postImportedExcel(StoreRequest $request)
    {
        $files = $request->files->all();
        $array = Excel::toArray(new QuestionsImport, $files['questionsFile']);
        $questionsInEachSection = [];
        $instruction  = Instruction::where('default', true )->first();

        if(!$instruction)
            return \Redirect::back()->withInput()->withError('No default Instructions set found. Please set a default "Instruction" set in the Master Settings to import your questions');

        $paper = QuestionPaper::firstOrNew([
            'name'=> $request->get('questionPaperName')
        ]);

//        if($paper->exists){
//            \Alert::error('There is already a question paper with the same name! Please enter a unique name')->flash();
//            return \Redirect::back()->withInput();
//        }
        $paper->instruction_id = $instruction->id;
        $paper->save();

        foreach ($array[0] as $key=>$row){ // looping each questions in the excel sheet

            if($key==0)
                continue;

//            [{"text":"<div>A does half as much work as B in one sixth of the time. If together they take 10 days to complete a work, how much time shall B take to do it alone?\r&nbsp;</div>","image":"","options":[{"text":"70 days"},{"text":"30 days"},{"answer":true,"text":"40 days"},{"text":"50 days"}]}]

            $subject = Subject::firstOrCreate(['name'=>$row[2]]);
            $sectionCategory = SectionCategory::firstOrCreate(['name'=>$row[3]]);

            $sectionCategoryId = strval($sectionCategory->id);
            $question = Question::firstOrNew([
                'question' => json_encode([
                    [
                        'text'=> '<div>'.$row[1].'</div>',
                        'image'=>'',
                        'options'=> [
                            [
                                'text'=> $row[5],
                                'answer'=> $row[21] == 'a'
                            ],
                            [
                                'text'=> $row[6],
                                'answer'=> $row[21] == 'b'
                            ],
                            [
                                'text'=> $row[7],
                                'answer'=> $row[21] == 'c'
                            ],
                            [
                                'text'=> $row[8],
                                'answer'=> $row[21] == 'd'
                            ]
                        ]
                    ]
                ]),
                'subject_id' => $subject->id,
                'section_category_id' => $sectionCategory->id,
                'question_summary' => mb_substr(strip_tags($row[1]), 0, 100),
                'difficulty'=> $row[25],
                'right_marks'=> $row[23],
                'negative_marks'=> $row[24],
                'solution'=> $row[22],
            ]);
            $question->save();
            $question->questionPapers()->save($paper);

            // Find and save TAGS
            $tags = explode(',', $row[26]); // get array of tags from the 27th column
            $tagArr = [];
            foreach ($tags as $key=>$tag){

                if($tag){
                    $t = Tag::firstOrCreate(['name'=> $tag]);
                    $tagArr[$key] = $t->id;
                }
            }
            $question->tags()->sync($tagArr);


            if(!in_array($sectionCategoryId, array_column($questionsInEachSection, 'section_id'))){
                array_push($questionsInEachSection, [
                    'section_id'=> $sectionCategoryId,
                    'questions'=> [
                        ['question_id' => strval($question->id)]
                    ]
                ]);
            } else if(in_array($sectionCategoryId, array_column($questionsInEachSection, 'section_id'))) {
                foreach ($questionsInEachSection as $key=>$q){
                    if($q['section_id']== $sectionCategoryId){
                        array_push($questionsInEachSection[$key]['questions'], ['question_id' => strval($question->id)] );
                    }
                };
            }
        }
//        dd('done');

        $paper->questions = json_encode($questionsInEachSection);
        $paper->save();

//        dd($questionsInEachSection);
        $exam = new Exam($request->all());
        $exam->sms_notifications = 1;
        $exam->accept_instructions = 1;
        $exam->question_paper_id = $paper->id;
        $exam->save();


//        $exam->examTypes()->attach($request->get('examTypes'));

        \Alert::success(trans('backpack::crud.insert_success'))->flash();
        $this->setSaveAction();

        return $this->performSaveAction();
        //        return view('vendor.backpack.crud.importExcel', $this->data);
    }
}
