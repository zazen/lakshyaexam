<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\DownloadRequest as StoreRequest;
use App\Http\Requests\DownloadRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class DownloadCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class DownloadCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Download');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/downloads');
        $this->crud->setEntityNameStrings('Download', 'Downloads');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        // add asterisk for fields that are required in DownloadRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');

        $this->crud->addFields([
            [
                'name'=> 'text',
                'label'=> 'Notification Text',
                'type'=> 'ckeditor',
            ],
//            [
//                'name'=> 'file_url',
//                'label'=> 'Upload your File',
//                'type'=> 'browse',
//                'hint'=> '',
//                'noJsLoad'=> false,
//                'allFileTypes' => true
//            ],
            [
                'name'=> 'batches',
                'label'=> 'Select Batches',
                'hint'=> 'Don\'t tick any of the above checkboxes if you want to allow this download for all batches',
                'type'=> 'checklist',
                'entity'=> 'batches',
                'attribute'=> 'name',
                'model'=> 'App\Models\Batch',
                'pivot'=> true,
                'select_all' => true,
            ],
        ]);
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
