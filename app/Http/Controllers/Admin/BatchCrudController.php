<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\BatchRequest as StoreRequest;
use App\Http\Requests\BatchRequest as UpdateRequest;
use Backpack\CRUD\CrudPanel;

/**
 * Class BatchCrudController
 * @package App\Http\Controllers\Admin
 * @property-read CrudPanel $crud
 */
class BatchCrudController extends CrudController
{
    public function setup()
    {
        /*
        |--------------------------------------------------------------------------
        | CrudPanel Basic Information
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Batch');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/batches');
        $this->crud->setEntityNameStrings('batch', 'batches');

        /*
        |--------------------------------------------------------------------------
        | CrudPanel Configuration
        |--------------------------------------------------------------------------
        */

        // TODO: remove setFromDb() and manually define Fields and Columns
        $this->crud->setFromDb();

        $this->crud->addFields([
            [
                'name'=> 'description',
                'label'=> 'Description/Comments (optional)',
//                'type'=> 'wysiwyg',
//                'hint'=> 'This field can be used to give the instructions of a reasoning/aptitude type question'
            ]
        ]);

        // add asterisk for fields that are required in BatchRequest
        $this->crud->setRequiredFields(StoreRequest::class, 'create');
        $this->crud->setRequiredFields(UpdateRequest::class, 'edit');
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }
}
