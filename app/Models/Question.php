<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class Question extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'questions';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
     protected $fillable = ['instructions', 'instructions_image', 'question','options','answer_option','question_category_id','exam_id','question_id','sub_questions', 'section_category_id','question_summary', 'difficulty', 'right_marks', 'negative_marks', 'solution', 'subject_id'];
    // protected $hidden = [];
    // protected $dates = [];
    protected $fakeColumns = ['options'];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

//    public function setQuestionsFileAttribute($value)
//    {
//        $attribute_name = "questionsFile";
//        $disk = "public";
//        $destination_path = "folder_1/subfolder_1";
//
//        $this->uploadFileToDisk($value, $attribute_name, $disk, $destination_path);
//    }

    public function getOptionsAttribute($value)
    {
        return (string)($value);
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function sectionCategory(){
        return $this->belongsTo('App\Models\SectionCategory');
    }

    public function questionPapers(){
        return $this->belongsToMany('App\Models\QuestionPaper', 'question_question_paper');
    }

    public function subject(){
        return $this->belongsTo('App\Models\Subject');
    }

    public function tags(){
        return $this->belongsToMany('App\Models\Tag', 'question_tag', 'question_id', 'tag_id');
    }

//    public function parentQuestion(){
//        return $this->belongsTo('App\Models\Question', 'question_id');
//    }

//    public function exam(){
//        return $this->belongsTo('App\Models\Exam');
//    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
