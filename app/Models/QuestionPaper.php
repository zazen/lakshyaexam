<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\CrudTrait;

class QuestionPaper extends Model
{
    use CrudTrait;

     /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    //protected $table = 'question_papers';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
     protected $fillable = [ 'name','instructions', 'no_of_questions', 'questions','question','options', 'answer_option','section_category_id','question_paper_id','answer_key_url', 'instruction_id', 'solutions_html'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function instruction(){
        return $this->belongsTo('App\Models\Instruction');
    }

    public function questions(){
        return $this->belongsToMany('App\Models\Question', 'question_question_paper');
    }

    public function tags(){
        return $this->belongsToMany('App\Models\Tag', 'question_paper_tag');
    }

    public function subjects(){
        return $this->belongsToMany('App\Models\Subject', 'question_paper_subject', 'question_paper_id', 'subject_id');
    }


    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
