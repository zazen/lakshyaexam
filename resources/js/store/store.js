import Vue from 'vue'
import Vuex from 'vuex'
import _ from 'lodash'
import moment from 'moment'
import swal from 'sweetalert'
import examResultTable from './examResultTable'

Vue.use(Vuex);

window._ = _;

const state = { exam: false, forceSubmitExam: false, user:false, sectionsWithQuestions: null, currentSection: null, questions: [], sections: [], subjects:[], answers: [], currentQuestionIndex: null, currentQuestion: null, acceptedExamInstructions: false, acceptedImportantInstructions:false, start_date_time: false, duration: false, now: false, remainingTime : false , examStartDateTime: false, answerModelInServer: false, completed: false, answerKeyUrl: '', finalResult: '', timeElapsed: false };


const getters = {

    getQuestions: state => _.compact(state.questions),

    getSectionsWithQuestions: state => _.compact(state.sectionsWithQuestions),

    currentSection: state => state.currentSection,

    getQuestionByIndex : (state, getters) => questionIndex => state.questions[questionIndex],

    currentQuestion : (state, getters) => state.currentQuestion,

    currentQuestionIndex : (state, getters) => state.currentQuestionIndex,

    currentQuestionId : (state, getters) => _.get(state.currentQuestion, 'question_id') || _.get(state.currentQuestion, 'id'),

    getAnswers : state => state.answers,

    getAnswer : state => questionId =>  _.find(state.answers, ans => ans.question_id == questionId ),

    checkIfExamInstructionsAccepted: state => state.acceptedExamInstructions,

    checkIfImportantInstructionsAccepted: state => state.acceptedImportantInstructions,

    getDuration : state => state.duration,

    ifExamIsCompleted: state => state.completed,

    finalResult: state => state.finalResult,

    user: state => state.user

};

const mutations = {

    INIT_EXAM : (state, {exam, sections, user, commit }, obj ) => {

        // lets check whether candidate has already started taking the exam
        //. pending

        state.exam = exam;
        state.user = user;
        console.log('user user user ', state)
        state.answerKeyUrl = _.get(exam,'question_paper.answer_key_url');

        console.log('INIT_EXAM exam', exam)
        let questionPaperQuestions = _.get(exam,'question_paper.questions');

        if(questionPaperQuestions){

            let sectionsWithQuestions = JSON.parse(questionPaperQuestions),
                sectionModels = JSON.parse(sections);

            console.log('sectionsWithQuestions', sectionsWithQuestions )
            sectionsWithQuestions = _.map( sectionsWithQuestions, (section, index) => {

                section['index'] = index
                section['section_name'] = _.get(_.find(sectionModels, s => s.id == section.section_id ), 'name')
                section['section_instructions'] = _.get(_.find(sectionModels, s => s.id == section.section_id ), 'default_instructions')
                if(index==0){
                    section['active'] = true
                    state.currentSection = section;
                }
                else section['active'] = false

                return section;
            } );
            state.sectionsWithQuestions= sectionsWithQuestions;
            // state.sectionsWithQuestions = _.map(sectionsWithQuestions, sec => {
            //     return sec;
            // });

            let questionIds = _.uniq(_.flattenDeep(sectionsWithQuestions.map(section => {

                    return section.questions.map(q =>  q.question_id )

                }))
            );
            const self = this;
            console.log('questionIds', questionIds, sectionsWithQuestions )

            axios.get('/api/questions',{ params: {questionIds: questionIds.join(',')} }).then(resp=> {
                commit('SET_SUBJECTS_FROM_SERVER', resp.data.subjects )
                setTimeout(()=>{
                    commit('SET_QUESTIONS_FROM_SERVER', exam.shuffle? _.shuffle(resp.data.questions): resp.data.questions )
                },500)
            })

        }

// Lets check how much time has elapsed for this user for this exam
        const UserData = localStorage.getItem(state.user.id)?  JSON.parse(localStorage.getItem(state.user.id)): {};

        state.now = moment(state.exam.now);
        const now = state.now;
        const now2 = state.now;
        state.start_date_time = UserData.timeElapsed? now.subtract(UserData.timeElapsed*1000): moment(state.exam.start_date_time);
        state.duration = state.exam.duration*60; // converting minutes into seconds

// moment returns this value in milliseconds
        state.remainingTime = moment(state.start_date_time.add(state.duration*1000)).diff(state.now);
        state.passedTIme = UserData.timeElapsed || now2.diff(state.start_date_time) ;
    },

    SUBMIT_EXAM: (state, {commit}) => {

        state.forceSubmitExam= true;
        commit('UPDATE_SECTION',{commit})
    },

    UPDATE_ANSWER: (state, { question_id, questionIndex, optionIndex, type } ) => {

        let answerIndex = _.findIndex(state.answers, ans => ans.question_id == question_id),
            question = _.find(state.questions,(q, index) => q.question_id == question_id),
            optionIndexOfAnswer = _.findIndex(question && question.options, option => option.answer );

        if(answerIndex == -1)
            state.answers.push({  questionIndex, question_id, optionIndex, optionIndexOfAnswer, type });
        else Vue.set(state.answers, answerIndex, { questionIndex, question_id, optionIndex, optionIndexOfAnswer, type});

        //Update the localStorage
        let userData = localStorage.getItem(state.user.id);

        if(userData){
            userData = ( userData && JSON.parse(userData) )? JSON.parse(userData): {}
            userData['answers'] = state.answers;
            console.log('userData in UPDATE_ANSWER', userData, state.user)
            localStorage.setItem(state.user.id, JSON.stringify(userData))

        }

        // Move onto the next question after above operations
    },

    UPDATE_CURRENT_QUESTION : (state, { currentQuestionIndex, closedSectionExam, goToPreviousQuestion }) => {


        let questions = state.questions;

        // If this is closed-section exam, we dont want the user to sktp to the questions to the next section unless the timer runs out
        // if(!state.exam.duration)
        //     questions = _.filter(questions, q => q.section_id ==  _.findIndex(state.sectionsWithQuestions, s => s.active) )
        currentQuestionIndex = currentQuestionIndex || 0;

        if(closedSectionExam){

            questions.forEach((q,index) => q.index = index);
            // get index of last question of this section
            let questionsInThisSection = _.filter(questions, q => +q.section_id == +_.get(state.currentSection, 'section_id') );
            let lastQuestionIndexOfCurrentSection = _.get(_.last(questionsInThisSection), 'index'),
                firstQuestionIndexOfCurrentSection = _.get(_.first(questionsInThisSection), 'index');

            if(!goToPreviousQuestion && (+currentQuestionIndex > +lastQuestionIndexOfCurrentSection))
                currentQuestionIndex = lastQuestionIndexOfCurrentSection

            if(goToPreviousQuestion && (+currentQuestionIndex < +firstQuestionIndexOfCurrentSection))
                currentQuestionIndex = firstQuestionIndexOfCurrentSection

        }

        if(questions[currentQuestionIndex]){
            state.currentQuestionIndex = currentQuestionIndex;
            state.currentQuestion = questions[currentQuestionIndex];
        }

        // In case, if this exam is an open-section exam, we need to highlight the activated section as well.
        if(state.exam.duration && _.isArray(state.sectionsWithQuestions))
            state.sectionsWithQuestions = _.map(state.sectionsWithQuestions, section => {
                section['active'] = state.currentQuestion && state.currentQuestion.section_id == section.section_id
                return section;
            })
                `   `
    },

    ACCEPT_EXAM_INSTRUCTIONS: (state, accepted) => { state.acceptedExamInstructions = accepted },

    ACCEPT_IMPORTANT_INSTRUCTIONS: (state, accepted ) => {

        state.acceptedImportantInstructions = accepted;

        console.log('state.user in ACCEPT_IMPORTANT_INSTRUCTIONS', state.user);
        // lets store the start time of the exam taken by the current user
        if(! localStorage.getItem(state.user.id))
            localStorage.setItem(state.user.id, JSON.stringify({ user_id: state.user.id, exam_id: state.exam.id, starttime: new Date() }) );

        axios.post('/api/answers',{ exam_id : state.exam.id, user_id: state.user.id } ).then(resp=> {

            state.start_date_time = resp.data.start_date_time.date;
            state.answerModelInServer = resp.data;
            // this.a.dispatch('setQuestionsFromServer', resp.data )
        })

    },

    SET_SUBJECTS_FROM_SERVER: (state, subjects) => {
        state.subjects = subjects;
    },

    SET_QUESTIONS_FROM_SERVER: (state, questions) => {

        let processedQuestions = _.map(questions, q => {

            q.question = JSON.parse(q.question)
            return q;

        });

        state.sectionsWithQuestions = _.map( state.sectionsWithQuestions, (section, sectionIndex) => {
            section['questions_in_this_section'] = _.filter( processedQuestions, q => {

                let subject = _.find(state.subjects, sub => +sub.id == +q.subject_id) || {};
                // console.log('q.subject_id', state.subjects, q.subject_id, subject, +(subject.section_category_id), +(section.section_id) )
                return +(subject.section_category_id) == +(section.section_id)
            } );
            return section;

        });

        let questionsToState = _.flattenDeep( _.map(state.sectionsWithQuestions, section => {

            return _.map(section.questions_in_this_section, qu => {

                return _.map(qu.question, (q,qIndex) => {

                    q['section_id'] = section.section_id;
                    q['question_id'] = qu.id;
                    q['section_name'] = section.section_name;
                    q['instructions'] = qu.instructions;
                    q['instructions_image'] = qu.instructions_image;
                    q['collapsed'] = qIndex == 0? false: true;

                    return q
                });

                return qu;
            })
        } ) );

        questionsToState = _.map(questionsToState, (q,index) => {
            q['number'] = index+1;
            return q
        });

        state.questions = questionsToState;

    },

    UPDATE_SECTION: (state, {sectionIndex, commit }) => {

        if(state.completed) return

        let indexOfCurrentActiveSection = state.forceSubmitExam? 999999999: _.findIndex(state.sectionsWithQuestions, section => section.active );

        let indexToBeUpdated = +sectionIndex >=0 ? +sectionIndex : ( indexOfCurrentActiveSection > -1 ? (indexOfCurrentActiveSection+1) : 0 );

        if(indexOfCurrentActiveSection <= -1)
            indexToBeUpdated = 0;

        // indexToBeUpdated++;
        let sectionToBeUpdated = state.sectionsWithQuestions[indexToBeUpdated]

        if(sectionToBeUpdated){
            sectionToBeUpdated['active'] = true;
            state.currentSection = sectionToBeUpdated;
            state.sectionsWithQuestions = _.map(state.sectionsWithQuestions, (sect, index) => {
                sect.active = index==indexToBeUpdated
                return sect;
            });
            // console.log('state.sectionsWithQuestions', state.sectionsWithQuestions)
            commit('UPDATE_CURRENT_QUESTION', {currentQuestionIndex: state.currentSection.questions_in_this_section[0].question[0].number-1} );
            // this.a.dispatch('updateCurrentQuestion', state.currentSection.questions_in_this_section[0].question[0].number-1 )
        }

        // return

        // if we have reached the end of all sections, then we need to alert the user and end the exam
        if(! sectionToBeUpdated){
            state.completed = true;

            swal({
                title: 'Preparing the Results',
                text: 'Please wait...',
                timer: 60000,
                showConfirmButton: false
            })
            let wrongAnswers = 0, correctAnswers = 0;

            let totalMarks = _.reduce( state.answers, (val, answer )=> {
                if(answer.type== 'answered' || answer.type == 'answeredMarkedForReview' ){

                    if(answer.optionIndex )
                        if(+answer.optionIndex == +answer.optionIndexOfAnswer)
                            correctAnswers++;
                        else wrongAnswers++;

                    return +val + ( answer.optionIndex ? (+answer.optionIndex == +answer.optionIndexOfAnswer ? +state.exam.marks_per_question : - +state.exam.negative_marks ) : 0 );
                }
                else return +val ;
            }, 0)

            axios.post('/api/answer/'+state.answerModelInServer.id, { exam_id: state.exam.id, user_id: state.user.id, answers: JSON.stringify(state.answers), finished: true, total_marks: totalMarks, wrong_answers: wrongAnswers, correct_answers: correctAnswers, exam_summary_html: examResultTable(state) } ).then(res => {
                // console.log(res.data)
                if(res.data){
                    console.log('state', state)
                    localStorage.removeItem(state.user.id)

                    state.finalResult += examResultTable(state);

                    setTimeout(()=>{

                        // console.log('get solution', document.getElementById('solution').innerHTML )
                        axios.post('/api/answer/'+state.answerModelInServer.id, { solution_html: document.getElementById('solution').innerHTML })
                            .then(resp =>console.log('resp', resp))
                            .catch(err => console.log('err', err ))

                    }, 1000)

                    //
                    // let x= setInterval(()=>{
                    //     if($('#solutions').length){
                    //         clearInterval(x);
                    //         axios.post('/api/question-paper/'+state.exam.question_paper_id, { 'solutions_html': $('#solutions').html() } )
                    //     }
                    // }, 2 * 1000 );


                    // return swal({
                    //     html: true,
                    //     title: 'Time over! '+ 'Your score is '+totalMarks + ' out of '+ state.exam.marks_per_question*state.questions.length ,
                    //     text: examResultTable(state),
                    //     type: 'success',
                    //     // timer: 100000,
                    //     showConfirmButton: true,
                    //     buttons:{
                    //         ok: {
                    //             value: 'confirm'
                    //         }
                    //     }
                    // }, isConfirm => {
                    //
                    //     // if(isConfirm)
                    //     //     location.href = '/';
                    //
                    // })
                }
                else return swal('Network issue', 'There seems to be a problem with the network! Please don\'t close this browser window. Refresh this page when internet connection is available!', 'error')

            }).catch(e => {

                console.log('e', e)
                swal('Network issue', 'There seems to be a problem with the network! Please don\'t close this browser window. Refresh this page when internet connection is available!', 'error')
            } )


        }

    },

    INIT_ANSWERS_FROM_LOCAL_STORAGE: state => {

        let userData = localStorage.getItem(state.user.id);

        if(userData) {
            userData = JSON.parse(userData);
            if(userData.answers)
                state.answers = userData.answers
        }

    },

    INCREMENT_TIME_ELAPSED: state => {

        const UserData = JSON.parse(localStorage.getItem(state.user.id));
        if(UserData){
            UserData['timeElapsed'] = state.timeElapsed || UserData.timeElapsed || 0;
            localStorage.setItem(state.user.id, JSON.stringify(UserData));
            state.timeElapsed = UserData.timeElapsed + 1;
        }
    }

};


const actions = {

    initExam : ( {commit},  ...props ) => {
        // console.log('props', ...props, Object.assign(...props, {commit}))
        return commit('INIT_EXAM', Object.assign(...props, {commit}) )
    },

    setQuestionsFromServer: ({commit}, questions ) => commit('SET_QUESTIONS_FROM_SERVER', questions),

    updateAnswer : ( {commit}, { question_id, questionIndex, optionIndex, type } ) => commit('UPDATE_ANSWER', { question_id, questionIndex, optionIndex, type }  ),

    updateCurrentQuestion : ({commit}, {currentQuestionIndex, closedSectionExam, goToPreviousQuestion } ) => commit('UPDATE_CURRENT_QUESTION', { currentQuestionIndex, closedSectionExam, goToPreviousQuestion } ),

    acceptExamInstructions : ({commit}, accepted ) => commit('ACCEPT_EXAM_INSTRUCTIONS', accepted ),

    acceptImportantInstructions : ({commit}, accepted ) => commit('ACCEPT_IMPORTANT_INSTRUCTIONS', accepted ),

    updateSection : ({commit}, {sectionIndex} ) => commit('UPDATE_SECTION' , { sectionIndex, commit } ),

    logout: ({commit}) => {

        Vue.http.post('/logout', response => response.json()).then(response => {

            swal('You have been logged out')

        })


    },

    initAnswerFromLocalStorage: ({commit}) => commit('INIT_ANSWERS_FROM_LOCAL_STORAGE'),

    incrementTimeElapsed: ({commit}) => commit('INCREMENT_TIME_ELAPSED'),

    submitExam: ({commit}) => commit('SUBMIT_EXAM', {commit} )
};


const exam = {
    state,
    getters,
    mutations,
    actions
};


export default new Vuex.Store({
    modules:{
        exam
    }
});
