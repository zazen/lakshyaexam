import _ from 'lodash'
import moment from 'moment'
import swal from 'sweetalert'
let state;

function questionIdsInSection(sectionId){
    let questions =  _.filter(state.questions, s => +s.section_id == +sectionId )
    return _.map(questions, q => q.question_id )
}

function  countOfAnswers(sectionId, types=[], exclude=false){
    let questionIds =  questionIdsInSection(sectionId)
    return _.reduce(_.filter(state.answers, ans => questionIds.includes(ans.question_id) ), (val,answer) => {

        if(exclude? ! types.includes(answer.type) : types.includes(answer.type)){
            return val+1
        }
        return val

    }, 0 )
}

function countOfCorrectIncorrectAnswers(sectionId, correct=true){
    let questionIds =  questionIdsInSection(sectionId)
    let count = _.reduce(_.filter(state.answers, ans => (ans.type== 'answered' || ans.type == 'answeredMarkedForReview') && questionIds.includes(ans.question_id) ), (val,answer) => {

        if(answer.optionIndex )
            if(correct && +answer.optionIndex == +answer.optionIndexOfAnswer)
                return val+1;
            else if(!correct && +answer.optionIndex !== +answer.optionIndexOfAnswer)
                return val+1;

        return val

    }, 0 )

    return count
}

function totalMarksOfQuestionPaper(){
    const { exam, answers, questions, sectionsWithQuestions } = state;
    const allQuestions = _.flattenDeep(_.map(sectionsWithQuestions, s => s.questions));

    let count = _.reduce(allQuestions, (val,question) => {
        return val+ parseFloat(question.marks || exam.marks_per_question )
    }, 0);

    return count;
}


function calculateMarks(sectionId) {
    const { exam, answers, questions, sectionsWithQuestions } = state;
    let questionIds =  questionIdsInSection(sectionId);
    // console.log('state', state)

    let count = _.reduce(_.filter(answers, ans => (ans.type== 'answered' || ans.type == 'answeredMarkedForReview') && questionIds.includes(ans.question_id) ), (val,answer) => {

        const section_id = _.get(_.find(questions, q => q.question_id == answer.question_id ), 'section_id'),
            section = _.find(sectionsWithQuestions, s => s.section_id == section_id ) || {},
            questionInSection = _.find(section.questions, q => parseInt(q.question_id) == parseInt(answer.question_id) ),
            marksOfQuestion = parseFloat(_.get(questionInSection, 'marks',0))

        if(answer.optionIndex)
            if(+answer.optionIndex == +answer.optionIndexOfAnswer)
                return val+(marksOfQuestion || exam.marks_per_question);
            else return val-exam.negative_marks;

        return val
    }, 0);

    return count
}

function finalMarks(){
    return _.reduce(state.sectionsWithQuestions, (val, section)=>{

        return val+ calculateMarks(section.section_id)

    }, 0 );
}

function passed(){
    const { exam, sectionsWithQuestions } = state;

    let passed=false;
    if(exam.cut_off_marks) { // not open-section exam

        passed = parseFloat(finalMarks()) >= exam.cut_off_marks;
    }
    else{ // if open section exam

        passed = _.every(sectionsWithQuestions, section => {

            let marksInThisSection = parseFloat(calculateMarks(section.section_id));
            return marksInThisSection >= section.cutoff_marks;

        })
    }
    return passed;
}

function message(){
    const { exam, sectionsWithQuestions } = state;
    return passed()? exam.passed_message || '' : exam.failed_message || '';
}

export default function showExamResultTable(st){

    state = st;

    const { exam, user, sectionsWithQuestions, currentSection, questions,ections,nswers,urrentQuestionIndex, currentQuestion, acceptedExamInstructions, acceptedImportantInstructions, start_date_time, duration, now, remainingTime, examStartDateTime, answerModelInServer, completed, answerKeyUrl } = state;

    let result = '<h2>Your answers have been submitted! '+ 'Your score is <span style="color:green">'+finalMarks() +'</span> out of '+ totalMarksOfQuestionPaper()+'</h2></br></br>';
    result +=  '<h2>'+message()+'</h2></br>';
    result += `<table class="table">
                                                    <thead>
                                                        <tr>
                                                            <th>Section</th>
                                                            <th>Attempted</th>
                                                            <th>Unattempted</th>
                                                            <th>Correct</th>
                                                            <th>Wrong</th>
                                                            <th>Cut-Off Marks</th>
                                                            <th>Your Marks</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    ${_.map(sectionsWithQuestions, section => {

        let finalMark
        return `<tr>
                                                        <td>${section.section_name}</td>
                                                        <td>${countOfAnswers(section.section_id, ['answered', 'answeredMarkedForReview'])}</td>
                                                        <td>${+questionIdsInSection(section.section_id).length - +countOfAnswers(section.section_id, ['answered', 'answeredMarkedForReview'])}</td>
                                                        <td>${ countOfCorrectIncorrectAnswers(section.section_id) }</td>
                                                        <td>${ countOfCorrectIncorrectAnswers(section.section_id, false) }</td>
                                                        <td>${exam.cut_off_marks? 'N/A' : section.cutoff_marks}</td>
                                                        <td>${ calculateMarks(section.section_id) }</td>
                                                        
                                                            </tr>`
    }).join('')}
                                                    
                                                    <tr class="strong">
                                                        <td>Total</td>
                                                        <td>
                                                            ${_.reduce(sectionsWithQuestions, (val, section)=>{

        return val+ +countOfAnswers(section.section_id, ['answered', 'answeredMarkedForReview'])

    }, 0 )}
                                                        </td>
                                                        <td>
                                                            ${_.reduce(sectionsWithQuestions, (val, section)=>{

        return val+ +questionIdsInSection(section.section_id).length - +countOfAnswers(section.section_id, ['answered', 'answeredMarkedForReview'])

    }, 0 )}
                                                        </td>
                                                        <td>
                                                            ${_.reduce(sectionsWithQuestions, (val, section)=>{

        return val+ countOfCorrectIncorrectAnswers(section.section_id)

    }, 0 )}
                                                        </td>
                                                        <td>
                                                            ${_.reduce(sectionsWithQuestions, (val, section)=>{

        return val+ countOfCorrectIncorrectAnswers(section.section_id, false)

    }, 0 )}
                                                        </td>
                                                        <td>${exam.cut_off_marks || 'N/A'} </td>
                                                        <td style="color:green">
                                                            ${finalMarks()}
                                                        </td>
                                                    </tr>
                                                    
                                                    </tbody>
                                                </table>`


    if(answerKeyUrl)
        result += `<div class="col-sm-offset-5 col-sm-2"> 
    <a class="an-btn an-btn-success" href="${answerKeyUrl}" target="_blank">View ANSWER KEY</a>
  </div>`

    return result
}
