import examResultTable from '../store/examResultTable'

export default{
    data(){
        return{
            acceptImportantInstructions: false, answerClasses : [], usr: null, minutesToBeAdded: 0, clock: null, intervalId: null,
            previousSectionIndex: 0,
            endTime: 0
        }
    },

    props: ['examobject','now','instructions','sections', 'user'],

    components:{
        instructions : require('./Instructions.vue').default,
        questionComponent : require('./QuestionComponent.vue').default,
    },

    mounted(){

        this.usr = JSON.parse(this.user)
        this.$store.dispatch('initExam', { exam: this.exam, sections: this.sections, user: this.usr });


        setTimeout(()=>{
            let userData = localStorage.getItem(this.usr.id)
            if(userData){
                userData = JSON.parse(userData)
//                    if(userData.answers)
//                        swal('Continue taking your exam', 'We have detected that you were already making progress with this exam! Your marked answers and reviews will now be restored', '')

                setTimeout(()=>{
                    this.$store.dispatch('acceptExamInstructions', true )
                    this.$store.dispatch('acceptImportantInstructions', true )
                    this.$store.dispatch('updateCurrentQuestion', 0 )
                    this.initAnswerFromLocalStorage()
                }, 900)
            }
//
        },1000);

        this.setQuestionIndex(0);

//            if(this.currentSection)
//                this.minutesToBeAdded = this.currentSection.time

        setTimeout(() => {
//                window.swal({
//                    html: true,
//                    title: '',
//                    text: examResultTable(this.$store.state.exam ),
//                    timer: 100000
//                }, isConfirm => {
//
//
//                })
        }, 1000)

        console.log('this.$store.state', this.$store.state)

        // from https://stackoverflow.com/questions/28690564/is-it-possible-to-remove-inspect-element
        document.onkeydown = function(e) {
            if(event.keyCode == 123) {
                return false;
            }
            if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
                return false;
            }
            if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)) {
                return false;
            }
            if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
                return false;
            }
            if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
                return false;
            }
        }

    },

    computed:{

        timer(){
            if(! this.checkIfImportantInstructionsAccepted )
                return;

            let startTimeOfUser = localStorage.getItem(this.usr.id),
                UserData = startTimeOfUser? JSON.parse(startTimeOfUser): {},
                sectionsCoveredFlag = false,
                bufferMinutesElapsed = 0;

            if(!this.exam.duration){ // if this is a session-time based exam
                bufferMinutesElapsed = _.reduce(this.getSectionsWithQuestions, (sum, section)=> {

                    if(!sectionsCoveredFlag && !section.active){
                        sum += +(section.time);
                    }
                    if(section.active){
                        sectionsCoveredFlag = true;
                    }
                    return sum;

                },0)
            }
            // console.log('bufferMinutesElapsed', bufferMinutesElapsed, UserData);

            // console.log('UserData', UserData)
            if(startTimeOfUser && JSON.parse(startTimeOfUser))
                startTimeOfUser = JSON.parse(startTimeOfUser).starttime;

            this.minutesToBeAdded = this.exam.duration || ( +this.currentSection.time );

            // console.log('startTimeOfUser', startTimeOfUser, bufferMinutesElapsed, this.minutesToBeAdded );
            // if(!this.exam.duration)
            //     this.minutesToBeAdded = +this.currentSection.time;

            this.endTime = new Date(startTimeOfUser).getTime()+ (this.minutesToBeAdded * 60 * 1000 ); // for open-section exams

            // for closed-section exams
            if(!this.exam.duration){
                this.endTime += (+bufferMinutesElapsed * 60 * 1000);

            }

            // if(UserData.timeElapsed)
            //     this.endTime -= +UserData.timeElapsed * 1000;
            // this.endTime = new Date( new Date().getTime()+ this.minutesToBeAdded*60*1000 - UserData.timeElapsed*1000 ).getTime();

            // $('#csrf_field'  ).val( document.head.querySelector('meta[name="csrf-token"]').content )
            $('#csrf_field').val( window.Laravel.csrfToken );

            // console.warn('this', this.exam.duration, +bufferMinutesElapsed * 60 * 1000, 'timeElapsed', UserData.timeElapsed, +this.currentSection.time ,'bufferMinutesElapsed', bufferMinutesElapsed, (+bufferMinutesElapsed * 60 * 1000), this.endTime  )

            if(!this.exam.duration && +this.currentSection.time && (this.currentSection.index !== this.previousSectionIndex) ){
                clearInterval(this.intervalId);
                this.intervalId = null;
                this.previousSectionIndex = this.currentSection.index;
            }

            // console.warn('this.intervalId', this.intervalId, 'this.currentSection.time', this.currentSection.time)
            if(!this.intervalId)
                setTimeout(() => {

                    this.intervalId =  setInterval( () => {

                        var distance = this.endTime - new Date().getTime();
                        // console.warn('this.endTime', this.endTime, new Date().getTime(), distance, this.intervalId )

                        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                        this.clock = hours+ ":" + minutes + ":" + (String(seconds).length == 1 ? '0'+seconds : seconds );
                        if (distance <= 1) {
                            // console.log('distance', distance, distance <= 1  )

                            clearInterval(this.intervalId);
                            this.intervalId = null;

                            setTimeout(()=>{
                                if(this.exam.duration){
                                    this.updateSection( this.exam.duration? 999999: null ); // if this is an open-section exam, then make the exam deliberately stop without proceeding
                                    this.clock = "";
                                }
                                else {
                                    // console.warn('+this.currentSection.index=====', +this.currentSection.index)
                                    this.updateSection(+this.currentSection.index + 1);
                                    this.clock = ""
                                }
                            })
                        }
                        else {
                            this.$store.dispatch('incrementTimeElapsed')
                        }


                    }, 1000);

                }, 300)
        },
        exam(){
            return JSON.parse( this.examobject );
        },
        // user(){
        //     return this.$store.getters.user;
        // },

        finalResult(){
            return this.$store.getters.finalResult
        },

        currentSection(){
            return this.$store.getters.currentSection
        },

        acceptedExamInstructions(){
            return this.$store.getters.checkIfExamInstructionsAccepted
        },

        acceptedImportantInstructions(){
            return this.$store.getters.checkIfImportantInstructionsAccepted
        },

        question(){ // currentQuestion computed function
            window.scrollTo(0,0,);
            setTimeout(()=>{
                if(!this.answer || !this.answer.optionIndex || ['unAnswered', undefined, false, null ].includes(this.answer.optionIndex) )
                    this.dispatchAnswerWithType( 'unAnswered' )
            },300);

            return this.$store.getters.currentQuestion || {}
        },

        questions(){

            // console.log('this.$store.getters.getQuestions', this.$store.getters.getQuestions)
            return _.filter(this.$store.getters.getQuestions, question => {

                return _.findIndex( this.getSectionsWithQuestions, section => (section.section_id == question.section_id) && section.active ) > -1
            } )
        },

        allQuestionsInThisExam() {
            return this.$store.getters.getQuestions
//              return   _.chain(this.getSectionsWithQuestions).map(section => section.questions_in_this_section).flattenDeep().map(que => que && que.question).flattenDeep().value()
        },
        questionIndex(){
            return this.$store.getters.currentQuestionIndex || 0
        },
        questionId(){
            // console.log('this.$store.getters.currentQuestionId', this.$store.getters.currentQuestionId)
            return this.$store.getters.currentQuestionId
        },
        getSectionsWithQuestions(){
            return this.$store.getters.getSectionsWithQuestions
        },
        answers(){
            return this.$store.getters.getAnswers
        },
        answer(){
            return this.$store.getters.getAnswer(this.questionId) || {}
        },

        totalQuestionsOfExam(){
            return _.reduce(this.getSectionsWithQuestions, ( num, section )=>{

                return num+ +this.questionsInThisSection(section).length;

            } , 0 )
        },

        totalMarksOfExam(){
            return _.reduce(this.getSectionsWithQuestions, ( num, section )=>{

                return num+ this.exam.marks_per_question * +this.questionsInThisSection(section).length;

            } , 0 )
        },

        sectionDuration(){
            return _.reduce(this.getSectionsWithQuestions, ( num, section )=>{

                return num+ parseInt(section.time);

            } , 0 )
        },

        totalDurationOfExam(){
            let time =  _.reduce(this.getSectionsWithQuestions, ( num, section )=>{
                return num+ +(section.time);

            } , 0 )

            return _.isNaN(time) ? this.exam.duration : time
        },

        ifExamIsCompleted() {

            return this.$store.getters.ifExamIsCompleted
        }

    },

    methods: {

        getAnswer(questionId){
            return this.$store.getters.getAnswer(questionId) || {}
        },

        finishAndSubmitExam(){

            this.$store.dispatch('submitExam')
            // setTimeout(()=>{ this.updateSection() }, 500 )
        },

        questionsInThisSection(section){
            return _.chain(section.questions_in_this_section).map(q =>  q.question).flattenDeep().value()
        },

        questionIdsInSection(sectionId){
            let questions =  _.filter(this.allQuestionsInThisExam, s => +s.section_id == +sectionId )
            return _.map(questions, q => q.question_id )
        },

        totalMarksOfQuestionPaper(){

            const allQuestions = _.flattenDeep(_.map(this.getSectionsWithQuestions, s => s.questions));

            let count = _.reduce(allQuestions, (val,question) => {
                return val+ parseFloat(question.marks || this.exam.marks_per_question )
            }, 0);

            return count;
        },

        totalMarksOfASection(sectionId){

            const allQuestions = _.flattenDeep(_.get(_.find(this.getSectionsWithQuestions, s => s.section_id ==  sectionId ), 'questions', []));

            let count = _.reduce(allQuestions, (val,question) => {
                return val+ parseFloat(question.marks || this.exam.marks_per_question )
            }, 0);

            return count;
        },

        countOfAnswers(sectionId, types=[], exclude=false){
            let questionIds =  this.questionIdsInSection(sectionId)

            return _.reduce(_.filter(this.answers, ans => questionIds.includes(ans.question_id) ), (val,answer) => {

                if(exclude? ! types.includes(answer.type) : types.includes(answer.type)){
                    return val+1
                }
                return val

            }, 0 )
        },

        unAttemptedAnswers(sectionId){
            let questions =  _.get(_.find(this.getSectionsWithQuestions, s => s.section_id == sectionId ), 'questions')

            return _.map(questions, q => q.question_id )
        },

        initAnswerFromLocalStorage(){
            this.$store.dispatch( 'initAnswerFromLocalStorage')
        },
        // Because we are using this same store action in multiple methods, its better to place it a single method

        dispatchAnswerWithType(type){

            console.log('type in dispatchAnswerWithType', type)

            this.$store.dispatch('updateAnswer' , { question_id: this.question.question_id, questionIndex: this.questionIndex, optionIndex: type !='unAnswered'? this.answer.optionIndex: false, type });

            if(['markedForReview','answeredMarkedForReview'].includes(type))
                this.setQuestionIndex(this.questionIndex+1)

        },

        updateAnswer(type='answered'){

            this.dispatchAnswerWithType(this.answer.optionIndex? type : 'unAnswered');
//                this.$store.dispatch('updateCurrentQuestion', this.questionIndex+1 )
            console.log('this.currentSection', type, this.questionIndex, this.currentSection, this.getSectionsWithQuestions);
            this.setQuestionIndex(this.questionIndex+1)
        },

        updateSection(sectionIndex){

            // lets proceed to the next section. This will increase the active section index by 1, by mutator in the store
            this.$store.dispatch('updateSection', {sectionIndex})

        },

        colors(index){

            return '#38A9EB';

        },

        setQuestionIndex(index, goToPreviousQuestion=false){
            let lastQuestionInThisSection = _.last(this.questions)
            if( this.exam.duration && lastQuestionInThisSection && lastQuestionInThisSection.number == index && _.last(this.allQuestionsInThisExam ).number == index )
                return swal('You have reached the end of the question paper. Please wait till the countdown stops')

            // console.warn('setQuestionIndex', index)
            this.$store.dispatch('updateCurrentQuestion', { currentQuestionIndex: index, closedSectionExam: !this.exam.duration, goToPreviousQuestion } )
        },

        // arg type should be of Array type
        getCountOfAnswerTypes(type){

            let questionIdsInThisSection = this.questions.map(q => q.question_id),
                answersForThisSection = this.answers.filter(answer =>  questionIdsInThisSection.includes(answer.question_id) )
            return _.filter( answersForThisSection, ans => type.includes(ans.type) ).length

        },

        getAnswerByQuestionIndex(questionIndex){

            return _.find(this.answers, ans => ans.questionIndex == questionIndex )
        },

        responseBlockClass(questionIndex){

            let answer  = this.getAnswerByQuestionIndex(questionIndex),
                bgImageUrl = '/assets/img/questions-sprite.png'

            switch(answer? answer.type: questionIndex ){
                case 'answered':
                    return  "background-image: url("+bgImageUrl+"); background-position: -5px -50px; border-radius: 0px;"
                    break;
                case 'unAnswered':
                    return  "background-image: url("+bgImageUrl+"); background-position: -39px -50px; border-radius: 0px;";
                    break;
                case 'markedForReview':
                    return "background-image: url("+bgImageUrl+"); background-position: -71px -50px; border-radius: 0px;"
                    break;

                case 'answeredMarkedForReview':
                    return "background-image: url("+bgImageUrl+"); background-position: -168px -55px; border-radius: 0px;"
                    break;
                default:
                    // not visited
                    return "background-image: url("+bgImageUrl+"); background-position: -104px -50px; border-radius: 0px; color: rgb(0, 0, 0);"
            }

        },

        checkIfExamInstructionsAccepted(){

            this.$store.dispatch('acceptExamInstructions', true );

        },

        goBackToExamInstructions(){

            this.$store.dispatch('acceptExamInstructions', false );
            setTimeout(() => {
                window.scrollTo(0,0)
            },200)

        },

        checkIfImportantInstructionsAccepted(){

            if(! this.acceptImportantInstructions)
                swal('Please accept our terms and conditions before proceeding!');
            else {

                this.$store.dispatch('acceptImportantInstructions', true );
                this.$store.dispatch('updateCurrentQuestion', 0 );
                swal('Exam started')
            }

        },

        clearResponse(){

            $('input[type="radio"]:checked').prop('checked', false );
            this.dispatchAnswerWithType('unAnswered')

        },


    }
}
