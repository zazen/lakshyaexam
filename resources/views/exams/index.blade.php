@extends('layouts.app')

@section('content')


    <div class="col-sm-8 col-sm-offset-2">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#list">Exam List</a></li>
            <li><a data-toggle="tab" href="#notifications">Active Notifications</a></li>
            <li><a data-toggle="tab" href="#results">My Results</a></li>
            <li><a data-toggle="tab" href="#downloads">My Downloads</a></li>
        </ul>

        <div class="tab-content">

            <div id="list" class="tab-pane fade in active">
                <div class="an-single-component with-shadow">
                    {{--<div class="an-component-header">--}}
                        {{--<h6>Exam List</h6>--}}
                        {{--<div class="component-header-right">--}}
                        {{--<form class="an-form" action="#">--}}
                        {{--<div class="an-search-field">--}}
                        {{--<input class="an-form-control" type="text" placeholder="Search...">--}}
                        {{--<button class="an-btn an-btn-icon" type="submit"><i class="icon-search"></i></button>--}}
                        {{--</div>--}}
                        {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="an-component-body">
                        <div class="an-user-lists customer-support">
                            <div class="an-lists-body an-customScrollbar">


                                @if(isset($allExams) and $allExams->count())
                                    @foreach($allExams as $exam)
                                        <a href="{{route('exams.show', ['id'=> $exam->id ] )}}">
                                            <div class="list-user-single closed">
                                                <div class="list-name">
                                                    {{--<span class="image"--}}
                                                    {{--style="background: url('assets/img/users/user8.jpeg') center center no-repeat;--}}
                                                    {{--background-size: cover;">--}}
                                                    {{--</span>--}}
                                                    <div class="col-sm-12">{{$exam->name}} </div>
                                                    <label class="label-info col-sm-12 uppercase">
                                                        {{$exam->difficulty}}
                                                    </label>
                                                    <div class="col-sm-12">
                                                        <small> Ends at  {{ \Carbon\Carbon::parse($exam->end_date_time)->format('h:m A d M')  }}.
                                                            <br> ({{ \Carbon\Carbon::parse($exam->end_date_time)->diffForHumans()  }}) </small>
                                                    </div>

                                                    <div class="list-state">
                                                        <span class="msg-tag read">Open</span>
                                                    </div>
                                                </div>
                                                <p class="comment">

                                                </p>
                                            </div> <!-- end .USER-LIST-SINGLE -->
                                        </a>
                                    @endforeach
                                @endif


                                {{--@if(isset($liveExams) and $liveExams->count())--}}
                                {{--@foreach($liveExams as $exam)--}}
                                {{--<a href="{{route('exams.show', ['id'=> $exam->id ] )}}">--}}
                                {{--<div class="list-user-single closed">--}}
                                {{--<div class="list-name">--}}
                                {{--<span class="image"--}}
                                {{--style="background: url('assets/img/users/user8.jpeg') center center no-repeat;--}}
                                {{--background-size: cover;">--}}
                                {{--</span>--}}
                                {{--<div class="col-sm-12">"{{$exam->name}}" </div> <br/>--}}
                                {{--<div class="col-sm-12">--}}
                                {{--<small> Ends at  {{ \Carbon\Carbon::parse($exam->end_date_time)->format('h:m A d M')  }}.--}}
                                {{--<br> ({{ \Carbon\Carbon::parse($exam->end_date_time)->diffForHumans()  }}) </small>--}}
                                {{--</div>--}}

                                {{--<div class="list-state">--}}
                                {{--<span class="msg-tag read">Open</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<p class="comment">--}}

                                {{--</p>--}}
                                {{--</div> <!-- end .USER-LIST-SINGLE -->--}}
                                {{--</a>--}}
                                {{--@endforeach--}}
                                {{--@endif--}}



                                {{--@if(isset($upcomingExams) and $upcomingExams->count())--}}
                                {{--@foreach($upcomingExams as $exam)--}}
                                {{--<a href="#">--}}
                                {{--<div class="list-user-single closed">--}}
                                {{--<div class="list-name">--}}
                                {{--<span class="image"--}}
                                {{--style="background: url('assets/img/users/user8.jpeg') center center no-repeat;--}}
                                {{--background-size: cover;">--}}
                                {{--</span>--}}
                                {{--{{$exam->name}}  &nbsp;<span> ({{ \Carbon\Carbon::parse($exam->start_date_time)->diffForHumans() }}) </span>--}}
                                {{--<div class="list-state">--}}
                                {{--<span class="msg-tag read">Open</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<p class="comment">--}}

                                {{--</p>--}}
                                {{--</div> <!-- end .USER-LIST-SINGLE -->--}}
                                {{--</a>--}}
                                {{--@endforeach--}}
                                {{--@endif--}}



                                {{--@if(isset($expiredExams) and $expiredExams->count())--}}
                                {{--@foreach($expiredExams as $exam)--}}
                                {{--<a href="#">--}}
                                {{--<div class="list-user-single closed">--}}
                                {{--<div class="list-name">--}}
                                {{--<span class="image"--}}
                                {{--style="background: url('assets/img/users/user8.jpeg') center center no-repeat;--}}
                                {{--background-size: cover;">--}}
                                {{--</span>--}}
                                {{--{{$exam->name}}  &nbsp;<span> ({{ \Carbon\Carbon::parse($exam->start_date_time)->diffForHumans() }}) </span>--}}
                                {{--<div class="list-state">--}}
                                {{--<span class="msg-tag read">Open</span>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                {{--<p class="comment">--}}

                                {{--</p>--}}
                                {{--</div> <!-- end .USER-LIST-SINGLE -->--}}
                                {{--</a>--}}
                                {{--@endforeach--}}
                                {{--@endif--}}


                            </div> <!-- end .AN-LISTS-BODY -->
                        </div>
                    </div> <!-- end .AN-COMPONENT-BODY -->
                </div>
            </div>

            <div id="notifications" class="tab-pane fade in">
                <div class="an-single-component with-shadow">
                    <div class="an-component-header">
                        <h6>Notifications</h6>
                    </div>
                    <div class="an-component-body">
                        <div class="an-user-lists customer-support">
                            <div class="an-lists-body an-customScrollbar">


                                @if(isset($notifications) and $notifications->count())
                                    @foreach($notifications as $item)

                                        <div class="list-user-single closed">
                                            <div class="list-name">
                                                {{--<span class="image"--}}
                                                {{--style="background: url('assets/img/users/user8.jpeg') center center no-repeat;--}}
                                                {{--background-size: cover;">--}}
                                                {{--</span>--}}
                                                <div class="col-sm-8">{!! $item->text !!}</div> <br/>
                                                <div class="col-sm-4">
                                                    <small> Posted at  {{ \Carbon\Carbon::parse($item->created_at)->format('h:m A d M, Y')  }}.
                                                        <br> ({{ \Carbon\Carbon::parse($item->created_at)->diffForHumans()  }}) </small>
                                                </div>

                                            </div>
                                            <p class="comment">

                                            </p>
                                        </div> <!-- end .USER-LIST-SINGLE -->

                                    @endforeach
                                @endif


                            </div> <!-- end .AN-LISTS-BODY -->
                        </div>
                    </div> <!-- end .AN-COMPONENT-BODY -->
                </div>
            </div>

            <div id="results" class="tab-pane fade">

                <div class="an-single-component with-shadow">
                    <div class="an-component-header">
                        {{--<div class="component-header-right">--}}
                        {{--<form class="an-form" action="#">--}}
                        {{--<div class="an-search-field">--}}
                        {{--<input class="an-form-control" type="text" placeholder="Search...">--}}
                        {{--<button class="an-btn an-btn-icon" type="submit"><i class="icon-search"></i></button>--}}
                        {{--</div>--}}
                        {{--</form>--}}
                        {{--</div>--}}
                    </div>
                    <div class="an-component-body">
                        <div class="an-user-lists customer-support">
                            <div class="an-lists-body an-customScrollbar">


                                @if(isset($answers) and $answers->count())
                                    @foreach($answers as $answer)
                                        <a data-toggle="modal" data-target="#answer_modal_{{$answer->id}}" >
                                            <div class="list-user-single closed">
                                                <div class="list-name">
                                                    {{--<span class="image"--}}
                                                    {{--style="background: url('assets/img/users/user8.jpeg') center center no-repeat;--}}
                                                    {{--background-size: cover;">--}}
                                                    {{--</span>--}}
                                                    <div class="col-sm-12"> {{$answer->exam? $answer->exam->name: '' }} </div>
                                                    <label class="label-info col-sm-12 uppercase">

                                                    </label>
                                                    <div class=S"col-sm-12">
                                                        {{$answer->exam? $answer->exam->difficulty: '' }}
                                                    </div>

                                                    <div class="list-state">
                                                        <span class="msg-tag read">View Solution</span>
                                                    </div>
                                                </div>
                                                <p class="comment">

                                                </p>
                                            </div> <!-- end .USER-LIST-SINGLE -->
                                        </a>


                                        <div class="modal fade" id="answer_modal_{{$answer->id}}" role="dialog">
                                            <div class="modal-dialog">

                                                <!-- Modal content-->
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        <h4 style="color:green;">
                                                            {{--<span class="glyphicon glyphicon-lock"></span>--}}
                                                            {{$answer->exam ? $answer->exam->name : '' }}
                                                        </h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        {!! $answer->solution_html !!}
                                                    </div>
                                                    <div class="modal-footer">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach
                                @endif

                            </div> <!-- end .AN-LISTS-BODY -->
                        </div>
                    </div> <!-- end .AN-COMPONENT-BODY -->
                </div>

            </div>


            <div id="downloads" class="tab-pane fade">
                <div class="an-single-component with-shadow">
                    <div class="an-component-header">
                        <h6>Downloads</h6>
                    </div>
                    <div class="an-component-body">
                        <div class="an-user-lists customer-support">
                            <div class="an-lists-body an-customScrollbar">


                                @if(isset($downloads) and $downloads->count())
                                    @foreach($downloads as $item)

                                        <div class="list-user-single closed">
                                            <div class="list-name">
                                                {{--<span class="image"--}}
                                                {{--style="background: url('assets/img/users/user8.jpeg') center center no-repeat;--}}
                                                {{--background-size: cover;">--}}
                                                {{--</span>--}}
                                                <div class="col-sm-8">{!! $item->text !!}</div> <br/>
                                                <div class="col-sm-4">
                                                    <small> Posted at  {{ \Carbon\Carbon::parse($item->created_at)->format('h:m A d M, Y')  }}.
                                                        <br> ({{ \Carbon\Carbon::parse($item->created_at)->diffForHumans()  }}) </small>
                                                </div>

                                            </div>
                                            <p class="comment">

                                            </p>
                                        </div> <!-- end .USER-LIST-SINGLE -->

                                    @endforeach
                                @endif


                            </div> <!-- end .AN-LISTS-BODY -->
                        </div>
                    </div> <!-- end .AN-COMPONENT-BODY -->
                </div>
            </div>
        </div>

    </div>



    <div class="col-md-8 col-md-offset-2 grid-item" >
        <!-- end .AN-SINGLE-COMPONENT messages -->
    </div>

@endsection
