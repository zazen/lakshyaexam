@if ($crud->hasAccess('update'))
	<a href="{{ url($crud->route.'/import') }} " class="btn btn-primary ladda-button"><i class="fa fa-file-excel-o"></i> Bulk Import Questions & Set Exam</a>
@endif
