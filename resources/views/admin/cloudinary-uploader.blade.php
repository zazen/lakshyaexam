@include('layouts.header')

<body>

<div id="app" class="main-wrapper">

    <div class="an-page-content m10top">

        {{--<div class="an-content-body home-body-content">--}}
        @yield('content')

        <div class="">
            {{--<cloudinary-uploader />--}}
            <button id="upload_widget" class="cloudinary-button">Upload files</button>


        </div> <!-- end .AN-PAGE-CONTENT-BODY -->
    </div> <!-- end .AN-PAGE-CONTENT -->

</div> <!-- end .MAIN-WRAPPER -->


@include('layouts.footer')
<script src="https://widget.cloudinary.com/v2.0/global/all.js" type="text/javascript"></script>

<script type="text/javascript">
    var myWidget = cloudinary.createUploadWidget({
            cloudName: 'my_cloud_name',
            uploadPreset: 'my_preset'}, (error, result) => {
            if (!error && result && result.event === "success") {
                console.log('Done! Here is the image info: ', result.info);
            }
        }
    )

    document.getElementById("upload_widget").addEventListener("click", function(){
        myWidget.open();
    }, false);

</body>

</html>
