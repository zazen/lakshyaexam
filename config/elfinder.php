<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Upload dir
    |--------------------------------------------------------------------------
    |
    | The dir where to store the images (relative from public).
    |
    */
    'dir' => ['uploads'],

    /*
    |--------------------------------------------------------------------------
    | Filesystem disks (Flysytem)
    |--------------------------------------------------------------------------
    |
    | Define an array of Filesystem disks, which use Flysystem.
    | You can set extra options, example:
    |
    | 'my-disk' => [
    |        'URL' => url('to/disk'),
    |        'alias' => 'Local storage',
    |    ]
    */
    'disks' => [
         'uploads',
    ],

    /*
    |--------------------------------------------------------------------------
    | Routes group config
    |--------------------------------------------------------------------------
    |
    | The default group settings for the elFinder routes.
    |
    */

    'route' => [
        'prefix'     => config('backpack.base.route_prefix', 'admin').'/elfinder',
        'middleware' => ['web', config('backpack.base.middleware_key', 'admin')], //Set to null to disable middleware filter
    ],

    /*
    |--------------------------------------------------------------------------
    | Access filter
    |--------------------------------------------------------------------------
    |
    | Filter callback to check the files
    |
    */

    'access' => 'Barryvdh\Elfinder\Elfinder::checkAccess',

    /*
    |--------------------------------------------------------------------------
    | Roots
    |--------------------------------------------------------------------------
    |
    | By default, the roots file is LocalFileSystem, with the above public dir.
    | If you want custom options, you can set your own roots below.
    |
    */

//    'roots' => null,
    'roots' => [
        [
            'driver' => 'FTP',
            'host'   => '104.152.168.21',
            'user'   => 'lakshyaftp@webdns.in',
            'pass'   => '5up3rU53r123#',
            'path'   => '/',
            'URL'        => 'http://webdns.in/lakshya_public_uploads/',
        ]
    ],
//        'roots' =>     [
//            'driver' => 'Flysystem',
//            'URL' => 'http://mydomain.com/content',
//            'alias' => 'Mydomain.com',
//            'filesystem' => new Filesystem(new FtpAdapter(
//                [
//                    'host' => 'mydomain.com',
//                    'username' => 'user',
//                    'password' => '****',
//                    'root' => '/domains/mydomain.com/public_html/content',
//                ]
//            )),
//        ],

    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    | These options are merged, together with 'roots' and passed to the Connector.
    | See https://github.com/Studio-42/elFinder/wiki/Connector-configuration-options-2.1
    |
    */

    'options' => [],

];
